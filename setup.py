#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-install

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
from os import path
import stat
import distutils
import glob
import sys
from distutils.core import setup, Extension
import distutils.command.build
import distutils.command.install
import distutils.command.install_egg_info

locales = ("ru","bg")

def cout(string):
    sys.stdout.write(string)
    sys.stdout.flush()

class build_po( distutils.core.Command ) :

    description = "build translation files"
    user_options = []

    def initialize_options( self ) :
        pass
    def finalize_options( self ) :
        pass

    def run( self ) :
        self.mkpath("build")
        #build.run( self )
        for locale in locales:
            for po in glob.glob("%s/*.po"%locale) :
                cmd = "msgfmt -c -o build/%s %s" % \
                    (path.basename(po)[:-2]+'mo',po)
                cout( cmd + "\n" )
                os.system(cmd)

class empty_egg_info( distutils.command.install_egg_info.install_egg_info ):
    def run(self):
        pass

class build( distutils.command.build.build ) :
    def run( self ) :
        distutils.command.build.build.run( self )

    def has_po( self ) :
        return len(glob.glob("ru/*.po")) > 0
    sub_commands = distutils.command.build.build.sub_commands + [
        ('build_po',has_po),
    ]

setup(
    name = "calculate-i18n",
    version = "2.2.20",
    description = "Calculate Linux internationalization",
    author = "Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    data_files = [("",(glob.glob("build/*.mo")))],
    cmdclass = {'build':build,'build_po':build_po,'install_egg_info':empty_egg_info}
)
